import React from 'react'
import './BasedButton.css'


interface BasedButtonProps {
  key: number,
  value: string | number,
  description: string,
  color: string,
  handleFunction(): void
}

const BasedButton: React.FC<BasedButtonProps> = (props) => {
  return(
    <div
      className = {`element button ${props.description} ${props.color}`}
      onClick = {() => props.handleFunction()}
    >
      {props.value}
    </div>
  )
}

export default BasedButton