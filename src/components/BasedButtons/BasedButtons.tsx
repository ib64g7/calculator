import React from 'react'
import './BasedButtons.css'
import BasedButton from '../BasedButton/BasedButton'
import calculator from '../../store/calculator'


type buttonProps = {
  value: string,
  description: string
  color: string,
  handleFunction: any
}

const BasedButtons: React.FC = () => {

  const buttons = [
    {
      value: 'AC',
      description: 'element-2',
      color: 'graphite',
      handleFunction: (): void => {
        calculator.clearResult()
      }
    },
    {
      value: '+/-',
      description: 'element-3',
      color: 'graphite',
      handleFunction: (): void => {
        if (calculator.displayValues.firstValue === '') return
        calculator.updateSymbolValue(!calculator.displayValues.symbol.display, '+/-')
        calculator.endResult()
      }
    },
    {
      value: '%',
      description: 'element-4',
      color: 'graphite',
      handleFunction: (): void => {
        if (calculator.displayValues.firstValue === '') return
        calculator.updateSymbolValue(!calculator.displayValues.symbol.display, '%')
        calculator.endResult()
      }
    },
    {
      value: '/',
      description: 'element-5',
      color: 'amber',
      handleFunction: (): void => {
        if (calculator.displayValues.firstValue === '') return
        calculator.updateSymbolValue(!calculator.displayValues.symbol.display, '/')
        calculator.changeOrderValue()
      }
    },
    {
      value: '7',
      description: 'element-6',
      color: 'graphite',
      handleFunction: (): void => {
        calculator.orderFirstValue ? calculator.updateFirstMember('7') : calculator.updateSecondMember('7')
      }
    },
    {
      value: '8',
      description: 'element-7',
      color: 'graphite',
      handleFunction: (): void => {
        calculator.orderFirstValue ? calculator.updateFirstMember('8') : calculator.updateSecondMember('8')
      }
    },
    {
      value: '9',
      description: 'element-8',
      color: 'graphite',
      handleFunction: (): void => {
        calculator.orderFirstValue ? calculator.updateFirstMember('9') : calculator.updateSecondMember('9')
      }
    },
    {
      value: 'x',
      description: 'element-9',
      color: 'amber',
      handleFunction: (): void => {
        if (calculator.displayValues.firstValue === '') return
        calculator.updateSymbolValue(!calculator.displayValues.symbol.display, 'x')
        calculator.changeOrderValue()
      }
    },
    {
      value: '4',
      description: 'element-10',
      color: 'graphite',
      handleFunction: (): void => {
        calculator.orderFirstValue ? calculator.updateFirstMember('4') : calculator.updateSecondMember('4')
      }
    },
    {
      value: '5',
      description: 'element-11',
      color: 'graphite',
      handleFunction: (): void => {
        calculator.orderFirstValue ? calculator.updateFirstMember('5') : calculator.updateSecondMember('5')
      }
    },
    {
      value: '6',
      description: 'element-12',
      color: 'graphite',
      handleFunction: (): void => {
        calculator.orderFirstValue ? calculator.updateFirstMember('6') : calculator.updateSecondMember('6')
      }
    },
    {
      value: '-',
      description: 'element-13',
      color: 'amber',
      handleFunction: (): void => {
        if (calculator.displayValues.firstValue === '') return
        calculator.updateSymbolValue(!calculator.displayValues.symbol.display, '-')
        calculator.changeOrderValue()
      }
    },
    {
      value: '1',
      description: 'element-14',
      color: 'graphite',
      handleFunction: (): void => {
        calculator.orderFirstValue ? calculator.updateFirstMember('1') : calculator.updateSecondMember('1')
      }
    },
    {
      value: '2',
      description: 'element-15',
      color: 'graphite',
      handleFunction: (): void => {
        calculator.orderFirstValue ? calculator.updateFirstMember('2') : calculator.updateSecondMember('2')
      }
    },
    {
      value: '3',
      description: 'element-16',
      color: 'graphite',
      handleFunction: (): void => {
        calculator.orderFirstValue ? calculator.updateFirstMember('3') : calculator.updateSecondMember('3')
      }
    },
    {
      value: '+',
      description: 'element-17',
      color: 'amber',
      handleFunction: (): void => {
        if (calculator.displayValues.firstValue === '') return
        calculator.updateSymbolValue(!calculator.displayValues.symbol.display, '+')
        calculator.changeOrderValue()
      }
    },
    {
      value: '0',
      description: 'element-18',
      color: 'graphite',
      handleFunction: (): void => {
        calculator.orderFirstValue ? calculator.updateFirstMember('0') : calculator.updateSecondMember('0')
      }
    },
    {
      value: ',',
      description: 'element-19',
      color: 'graphite',
      handleFunction: (): void => {
        if (calculator.displayValues.firstValue === '') return
        calculator.orderFirstValue ? calculator.updateFirstMember('.') : calculator.updateSecondMember('.')
      }
    },
    {
      value: '=',
      description: 'element-20',
      color: 'amber',
      handleFunction: (): void => {
        if (calculator.displayValues.firstValue === '') return
        calculator.endResult()
      }
    }
  ] as Array<buttonProps>


  return(
    <>
      {
        buttons.map((button, index) => {
          return(
            <BasedButton
              key = {index}
              value = {button.value}
              description = {button.description}
              color = {button.color}
              handleFunction = {button.handleFunction}
            />
          )
        })
      }
    </>
  )
}

export default BasedButtons