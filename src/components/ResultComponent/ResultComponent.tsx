import './ResultComponent.css'
import calculator from '../../store/calculator'
import { observer } from 'mobx-react-lite'

const ResultComponent: React.FC = observer(() => {
  return(
    <div className = "element element-1 result-component">
      <div>
        {calculator.displayValues.firstValue === '' ? '0' : calculator.displayValues.firstValue}
      </div>
      <div>
        {calculator.displayValues.symbol.value}
      </div>
      <div>
        {calculator.displayValues.lastValue.value}
      </div>
    </div>
  )
})

export default ResultComponent