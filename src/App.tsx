import React from 'react'
import './App.css'

import ResultComponent from './components/ResultComponent/ResultComponent'
import BasedButtons from './components/BasedButtons/BasedButtons'

const App: React.FC = () => {
  return (
    <div className = "container">
      <div className = "App">
        <div className = "wrapper">
          <ResultComponent />
          <BasedButtons />
        </div>
      </div>
    </div>
  );
}

export default App
