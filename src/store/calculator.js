import { makeAutoObservable } from "mobx"

class Calculator {

  displayValues = {
    firstValue: '',
    symbol: {
      display: false,
      value: ''
    },
    lastValue: {
      display: false,
      value: ''
    }
  }

  orderFirstValue = true

  constructor() {
    makeAutoObservable(this)
  }

  updateSymbolValue(display, value) {
    this.displayValues.symbol.display = display
    this.displayValues.symbol.value = value
  }

  add() {
    return String(Number(this.displayValues.firstValue) + Number(this.displayValues.lastValue.value))
  }

  deduct() {
    this.displayValues.symbol.display = true
    this.displayValues.symbol.value = '-'
    return String(Number(this.displayValues.firstValue) - Number(this.displayValues.lastValue.value))
  }

  multiply() {
    this.displayValues.symbol.display = true
    this.displayValues.symbol.value = '*'
    return String(Number(this.displayValues.firstValue) * Number(this.displayValues.lastValue.value))
  }

  share() {
    this.displayValues.symbol.display = true
    this.displayValues.symbol.value = '/'
    return String(Number(this.displayValues.firstValue) / Number(this.displayValues.lastValue.value))
  }

  percentage() {
    this.displayValues.symbol.value = ''
    return String(Number(this.displayValues.firstValue) / 100)
  }

  changeCharge() {
    this.displayValues.symbol.value = ''
    if (this.displayValues.firstValue.includes('-')) {
      this.displayValues.firstValue = this.displayValues.firstValue.replace(/-/, '')
    } else this.displayValues.firstValue = `-${this.displayValues.firstValue}`
  }

  updateFirstMember(value) {
    this.firstMember += value
    this.displayValues.firstValue += value
  }

  updateSecondMember(value) {
    this.secondMember += value
    this.displayValues.lastValue.value += value 
  }

  changeOrderValue() {
    this.orderFirstValue = !this.orderFirstValue
  }

  clearResult() {
    this.displayValues.firstValue = ''
    this.displayValues.symbol.display = false
    this.displayValues.symbol.value = ''
    this.displayValues.lastValue.display = false
    this.displayValues.lastValue.value = ''
  }

  clearDisplayValues() {
    this.displayValues.symbol.value = ''
    this.displayValues.lastValue.value = ''
  }

  endResult() {
    switch(this.displayValues.symbol.value) {
      case '+':
        this.displayValues.firstValue = this.add()
        this.clearDisplayValues()
        this.changeOrderValue()
        break
      case '-':
        this.displayValues.firstValue = this.deduct()
        this.clearDisplayValues()
        this.changeOrderValue()
        break
      case 'x':
        this.displayValues.firstValue = this.multiply()
        this.clearDisplayValues()
        this.changeOrderValue()
        break
      case '/':
        this.displayValues.firstValue = this.share()
        this.clearDisplayValues()
        this.changeOrderValue()
        break
      case '%':
        this.displayValues.firstValue = this.percentage()
        break
      case '+/-':
        this.changeCharge()
        break
    }
  }
}

export default new Calculator()